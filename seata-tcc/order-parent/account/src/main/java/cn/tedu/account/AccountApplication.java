package cn.tedu.account;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.PlatformTransactionManager;

import javax.annotation.PostConstruct;

@MapperScan("cn.tedu.account.mapper")
@SpringBootApplication
public class AccountApplication {
	@Autowired
	PlatformTransactionManager ptm;

	public static void main(String[] args) {
		SpringApplication.run(AccountApplication.class, args);
	}

	@PostConstruct
	public void test() {
		System.out.println("--------------------------------------"+ptm);
	}

}
