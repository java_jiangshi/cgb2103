package cn.tedu.account.service;
import cn.tedu.account.mapper.AccountMapper;
import cn.tedu.account.tcc.AccountTccAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountTccAction accountTccAction;
    @Override
    public void decrease(Long userId, BigDecimal money) {
        accountTccAction.prepare(null, userId, money);
    }
}
