package cn.tedu.order.tcc;

import cn.tedu.order.entity.Order;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

import java.math.BigDecimal;

/*
这个接口用来包含TCC三个操作方法
第一阶段： Try       - prepare()
第二阶段：
         Confirm   - commit()
         Cancel    - rollback()
 */
@LocalTCC
public interface OrderTccAction {
    /*
    为了避开 seata 的一个bug，这里不使用封装对象，
    而是一个一个的接收订单数据
     */
    @TwoPhaseBusinessAction(name = "OrderTccAction")
    boolean prepare(BusinessActionContext ctx,
                    // 把id放入上下文对象，向第二阶段传递
                    @BusinessActionContextParameter(paramName = "id") Long id,
                    Long userId,
                    Long productId,
                    Integer count,
                    BigDecimal money);

    boolean commit(BusinessActionContext ctx);
    boolean rollback(BusinessActionContext ctx);
}
