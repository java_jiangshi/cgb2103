package cn.tedu.order.tcc;

import cn.tedu.order.entity.Order;
import cn.tedu.order.mapper.OrderMapper;
import io.seata.rm.tcc.api.BusinessActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Component
public class OrderTccActionImpl implements OrderTccAction {
    @Autowired
    private OrderMapper orderMapper;

    @Transactional
    @Override
    public boolean prepare(BusinessActionContext ctx, Long id, Long userId, Long productId, Integer count, BigDecimal money) {
        orderMapper.create(new Order(id,userId,productId,count,money,0));

        // 添加一阶段成功标记
        ResultHolder.setResult(OrderTccAction.class, ctx.getXid(), "P");
        return true;
    }

    @Transactional
    @Override
    public boolean commit(BusinessActionContext ctx) {
        // 标记不存在，二阶段不再重复执行
        if (ResultHolder.getResult(OrderTccAction.class, ctx.getXid()) == null) {
            return true;
        }

        Long id = Long.valueOf(ctx.getActionContext("id").toString());
        orderMapper.updateStatus(id, 1);

        //二阶段成功，删除标记
        ResultHolder.removeResult(OrderTccAction.class, ctx.getXid());
        return true;
    }

    @Transactional
    @Override
    public boolean rollback(BusinessActionContext ctx) {
        if (ResultHolder.getResult(OrderTccAction.class, ctx.getXid()) == null) {
            return true;
        }
        Long id = Long.valueOf(ctx.getActionContext("id").toString());
        orderMapper.deleteById(id);
        ResultHolder.removeResult(OrderTccAction.class, ctx.getXid());
        return true;
    }
}
