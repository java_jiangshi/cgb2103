package cn.tedu.sp06.filter;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.jnlp.FileContents;
import javax.servlet.http.HttpServletRequest;

/*
模拟判断是否登录

没有登录不允许访问：
http://localhost:3001/item-service/o87uj

已登录可以访问：
http://localhost:3001/item-service/o87uj?token=8k7j6h5

只检查对商品访问的权限
用户和订单的访问不检查权限
 */
@Component
public class LoginFilter extends ZuulFilter {
    // 过滤器的类型： pre, routing, post, error
    @Override
    public String filterType() {
        // 前置过滤器，一般自定义过滤器都用前置过滤器
        // return "pre";
        return FilterConstants.PRE_TYPE;
    }

    // 设置过滤器的顺序号
    @Override
    public int filterOrder() {
        // 第5个过滤器中添加了serviceId
        // 后面过滤器中才能使用
        return 6;
    }

    // 判断针对当前请求，是否执行过滤代码
    @Override
    public boolean shouldFilter() {
        // 调用商品检查权限
        // 调用用户和订单不检查权限

        // 判断当前请求调用的是否是 item-service

        // 1. 获得一个请求上下文对象
        RequestContext ctx =
                RequestContext.getCurrentContext();
        // 2. 从上下文对象获取调用的服务id
        String serviceId =
            (String) ctx.get(FilterConstants.SERVICE_ID_KEY);// "serviceId"
        // 3. 判断服务id
        return "item-service".equals(serviceId);

    }

    // 过滤代码
    @Override
    public Object run() throws ZuulException {
        // 1.获得上下文对象
        RequestContext ctx =
                RequestContext.getCurrentContext();
        // 2.获得request对象
        HttpServletRequest request = ctx.getRequest();
        // 3.接收 token 参数
        String token = request.getParameter("token");
        // 4.如果没有token,阻止继续调用,直接返回响应
        if (StringUtils.isBlank(token)) {
            // 阻止继续调用
            ctx.setSendZuulResponse(false);
            // 直接返回响应
            // JsonResult --> {code:400,msg:Not Login,data:null}
            String json = JsonResult
                            .err()
                            .code(JsonResult.NOT_LOGIN)
                            .msg("Not Login! 未登录！")
                            .toString();
            ctx.addZuulResponseHeader(
                    "Content-Type",
                    "application/json;charset=UTF-8");
            ctx.setResponseBody(json);

        }
        // 在当前zuul版本中，这个返回值没有任何作用
        return null;
    }
}
