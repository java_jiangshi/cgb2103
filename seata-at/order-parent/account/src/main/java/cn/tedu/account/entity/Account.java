package cn.tedu.account.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private Long id;
    private Long userId; //用户id
    private BigDecimal total; //总金额
    private BigDecimal used;  //已使用金额
    private BigDecimal residue;  //可用金额
    private BigDecimal frozen;   //冻结金额
}
