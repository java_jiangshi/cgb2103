package cn.tedu.storage.controller;

import cn.tedu.storage.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class StorageController {
    @Autowired
    private StorageService storageService;

    @GetMapping("/decrease")   //    ?productId=1&count=10
    public String decrease(Long productId, Integer count) {
        storageService.decrease(productId, count);
        return "减少库存成功";
    }
}
