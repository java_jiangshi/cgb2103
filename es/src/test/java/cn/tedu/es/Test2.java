package cn.tedu.es;

import cn.tedu.es.entity.Student;
import cn.tedu.es.repo.StudentSearcher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import java.util.List;

@SpringBootTest
public class Test2 {
    @Autowired
    private StudentSearcher s;

    @Test
    public void test1() {
        List<Student> list = s.findByBirthDate("2020-08-19");
        System.out.println(list);
    }

    @Test
    public void test2() {
        PageRequest pageable = PageRequest.of(1, 2);

        List<Student> list = s.findByBirthDate(
                "2000-01-01", "2022-01-01", pageable);
        System.out.println(list);
    }


}
