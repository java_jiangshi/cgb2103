package cn.tedu.es;

import cn.tedu.es.entity.Student;
import cn.tedu.es.repo.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class Test1 {
    /*
    使用 respository 访问 es 数据库，
    和 Mapper 作用相同，访问底层存储
     */
    @Autowired
    private StudentRepository sr;

    @Test
    public void test1() {
        sr.save(new Student(9527L,"唐伯虎",'男',"2012-08-05"));
        sr.save(new Student(9528L,"祝枝山",'男',"2011-11-28"));
        sr.save(new Student(9529L,"华夫人",'女',"2012-05-17"));
        sr.save(new Student(9520L,"如花",'女',"2011-10-09"));
    }


    @Test
    public void test2() {
        sr.save(new Student(9528L,"刘德华",'男',"2020-08-19"));
    }


    @Test
    public void test3() {
        Optional<Student> op = sr.findById(9527L);
        Student s = op.get();
        System.out.println(s);

        System.out.println("---------------------------------");

        Iterable<Student> it = sr.findAll();
        for (Student s2 : it) {
            System.out.println(s2);
        }
    }

    @Test
    public void test4() {
        sr.deleteById(9520L);
    }

    @Test
    public void test5() {
        // Pageable 的子类型
        PageRequest pageable = PageRequest.of(0, 10);

        Page<Student> page = sr.findByName("唐", pageable);
        System.out.println("最大页： "+page.getTotalPages());
        System.out.println("是否有上一页："+page.hasPrevious());
        System.out.println("是否有下一页："+page.hasNext());
        for (Student s : page) {
            System.out.println(s);
        }
    }
    @Test
    public void test6() {
        List<Student> list = sr.findByNameOrBirthDate("唐", "2020-08-19");
        for (Student s : list) {
            System.out.println(s);
        }
    }
}
