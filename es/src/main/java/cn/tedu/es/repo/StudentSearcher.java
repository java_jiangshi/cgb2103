package cn.tedu.es.repo;

import cn.tedu.es.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
Criteria
        用来设置搜索条件
CriteriaQuery
        查询对象，封装上面的条件，和分页参数
SearchOperation
        用来执行相面的查询
 */
@Component
public class StudentSearcher {
    @Autowired
    private ElasticsearchOperations op;

    public List<Student> findByBirthDate(String birthDate) {
        Criteria c = new Criteria("birthDate");
        c.is(birthDate);
        return exec(c, null);
    }
    public List<Student> findByBirthDate(String from, String to, Pageable pageable) {
        Criteria c = new Criteria("birthDate");
        c.between(from, to);
        return exec(c, pageable);
    }

    private List<Student> exec(Criteria c, Pageable pageable) {
        // 条件封装成Query对象
        CriteriaQuery q = new CriteriaQuery(c);
        if (pageable != null) {
            q.setPageable(pageable);
        }

        // 用 searchoperation 执行查询
        SearchHits<Student> sh = op.search(q, Student.class);
        // 集合api
        // List<Student> list = new ArrayList<>();
        // for (SearchHit<Student> hit : sh) {
        //     Student stu = hit.getContent();
        //     list.add(stu);
        // }
        // 集合的流操作api
        List<Student> list = sh
                .stream()
                .map(SearchHit::getContent)
                .collect(Collectors.toList());
        return list;
    }
}
