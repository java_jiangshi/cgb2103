package cn.tedu.es.repo;

import cn.tedu.es.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import java.util.List;

/*
spring data elasticsearch 的 Repository API,
需要继承 ElasticsearchRepository 父接口，
在父接口中已经定义了基本数据增删改查操作方法。

spring 的 repository 规范，不需要自己写代码或添加配置，
只需要定义抽象接口，spring会自动创建动态代理对象

分页工具
    Pageable: 向服务器发送的分页参数，页号（从0开始）、每页大小
    Page: (可选) 封装服务器返回的结果，当前页的数据、分页属性


 */
public interface StudentRepository
        extends ElasticsearchRepository<Student, Long> {
    // 在name字段搜索
    Page<Student> findByName(String key, Pageable pageable);

    // 在name或出生日期中搜索
    List<Student> findByNameOrBirthDate(
            String name, String birthDate);
}
