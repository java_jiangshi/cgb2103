package cn.tedu.es.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

// Student 存储的索引
// 如果索引不存在，可以根据这里的参数自动创建索引、映射
// 索引和映射一般在服务器上手动创建
@Document(indexName = "students",shards = 3,replicas = 2)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id   // 用学生学号作为索引id
    private Long id;
    private String name;
    private Character gender;
    @Field("birthDate") // 索引中字段的名字
    private String birthDate;
}
