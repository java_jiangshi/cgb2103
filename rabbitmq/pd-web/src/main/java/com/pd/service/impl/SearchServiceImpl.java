package com.pd.service.impl;
import com.pd.es.ItemRepository;
import com.pd.pojo.Item;
import com.pd.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private ItemRepository ir;

    @Override
    public Page<Item> search(String key, Pageable pageable) {
        return ir.findByTitleOrSellPoint(key, key, pageable);
    }
}
