package com.pd.controller;

import com.pd.pojo.Item;
import com.pd.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SearchController {
    @Autowired
    private SearchService searchService;
    /*
    https://docs.spring.io/spring-data/elasticsearch/docs/4.2.3/reference/html/#core.web.basic
    toSearch.html?key=手机&page=0&size=10
     */
    @GetMapping("/search/toSearch.html")   //  ?key=手机
    public String search(String key, Pageable pageable, Model model) {
        Page<Item> page = searchService.search(key, pageable);

        //  model中的数据会向 jsp 传递，jsp中使用名称"page"访问数据
        model.addAttribute("page", page);
        return "/search.jsp";
    }
}
