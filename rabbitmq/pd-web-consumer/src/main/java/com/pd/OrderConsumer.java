package com.pd;

import com.pd.pojo.PdOrder;
import com.pd.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// 从指定的队列接收消息，自动反序列化成 order 对象
@RabbitListener(queues = "orderQueue")
@Component
public class OrderConsumer {
    @Autowired
    private OrderService orderService;

    // 配合 @RabbitListener，用来指定处理消息的方法
    @RabbitHandler
    public void receive(PdOrder order) throws Exception {
        orderService.saveOrder(order);
        System.out.println("-------------------------订单已保存");
    }
}
