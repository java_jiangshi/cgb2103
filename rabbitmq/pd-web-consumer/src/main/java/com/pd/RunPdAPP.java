package com.pd;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("com.pd.mapper")
public class RunPdAPP{
	
	public static void main(String[] args) {
		SpringApplication.run(RunPdAPP.class, args);
	}

	/*
	使用 spring 封装的 Queue 对象，提供队列参数，

	RabbitAutoConfiguration 自动配置类中，会在 spring 容器中
	自动发现 Queue 实例，使用它的参数连接服务器创建队列
	 */
	// import org.springframework.amqp.core.Queue;
	@Bean
	public Queue orderQueue() {
		return new Queue("orderQueue",true,false,false);
	}

}
