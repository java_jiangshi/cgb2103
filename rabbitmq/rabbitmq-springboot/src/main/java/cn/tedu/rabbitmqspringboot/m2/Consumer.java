package cn.tedu.rabbitmqspringboot.m2;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    // 每个 @RabbitListener 注解都会注册成为一个消费者
    @RabbitListener(queues = "task_queue")
    public void receive1(String s) {
        System.out.println("消费者1收到： "+s);
    }

    @RabbitListener(queues = "task_queue")
    public void receive2(String s) {
        System.out.println("消费者2收到： "+s);
    }
}
