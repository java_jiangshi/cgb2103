package m4;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        // 创建 Fanout 类型交换机： logs
        // c.exchangeDeclare("logs", "fanout");
        c.exchangeDeclare("direct_logs", BuiltinExchangeType.DIRECT);

        // 向 logs 交换机发送消息
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            System.out.print("输入路由键：");
            String k = new Scanner(System.in).nextLine();

            // 对默认交换机，路由键就是队列名
            c.basicPublish("direct_logs", k, null, s.getBytes());
        }

    }
}
