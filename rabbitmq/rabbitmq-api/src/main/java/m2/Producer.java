package m2;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        // 创建队列 helloworld
        c.queueDeclare("task_queue", true, false, false, null);

        // 发送消息
        while (true) {
            System.out.print("输入消息： ");
            String s = new Scanner(System.in).nextLine();
            // c.basicPublish("", "task_queue", 添加持久参数, s.getBytes());
            c.basicPublish("", "task_queue", MessageProperties.PERSISTENT_TEXT_PLAIN, s.getBytes());
        }

    }
}
