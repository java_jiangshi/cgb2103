package m1;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setUsername("admin");
        f.setPassword("admin");
        Channel c = f.newConnection().createChannel();

        // 创建队列 helloworld
        c.queueDeclare("helloworld", false, false, false, null);

        // 创建回调对象
        DeliverCallback deliverCallback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                byte[] a = message.getBody();
                String s = new String(a, "UTF-8");
                System.out.println("收到： "+s);
            }
        };
        CancelCallback cancelCallback = new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {
            }
        };
        // 开始接收消息，收到的消息会传递到一个回调对象进行处理
        // c.basicConsume("helloworld",true,处理消息的回调对象,取消消息处理时的回调对象);
        /*
        第二个参数 autoAck
        - autoAck=true    自动确认（Acknowledgment）
        - autoAck=false   手动确认
         */
        c.basicConsume("helloworld",true,deliverCallback,cancelCallback);
    }
}
