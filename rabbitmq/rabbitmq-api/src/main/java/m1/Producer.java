package m1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");
        f.setPort(5672); //默认端口，可以不设置
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection(); //连接
        Channel c = con.createChannel();    //通信通道
        // 创建队列 helloworld
        // 如果队列已经存在，不会重复创建
        /*
        参数：
           1. 队列名
           2. 是否是持久队列
           3. 是否是排他队列（独占）
           4. 是否自动删除
           5. 其他参数属性设置
         */
        c.queueDeclare("helloworld",false,false,false,null);
        // 向 helloworld 队列发送消息
        /*
        参数：
           1. ""是一个默认的交换机
           3. 消息的其他参数属性
         */
        c.basicPublish("", "helloworld", null, "中文Hello world!".getBytes("UTF-8"));

    }
}
