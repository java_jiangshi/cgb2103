package redis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

public class Test1 {
    public static void main(String[] args) {
        // jedis 客户端api
        // 使用一致性哈希算法进行分片访问
        JedisPoolConfig cfg = new JedisPoolConfig();
        List<JedisShardInfo> list = new ArrayList<>();
        list.add(new JedisShardInfo("192.168.64.150", 7000));
        list.add(new JedisShardInfo("192.168.64.150", 7001));
        list.add(new JedisShardInfo("192.168.64.150", 7002));
        // 常见连接池
        ShardedJedisPool pool = new ShardedJedisPool(cfg, list);
        ShardedJedis j = pool.getResource();
        for (int i = 0; i < 100; i++) {
            j.set("k"+i, "v"+i);
        }

    }
}
