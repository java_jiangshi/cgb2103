package cn.tedu.account.tx;


import cn.tedu.account.entity.AccountMessage;
import cn.tedu.account.service.AccountService;
import cn.tedu.account.util.JsonUtil;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@RocketMQMessageListener(topic = "order-topic",
                         consumerGroup = "account-consumer")
@Component
public class AccountConsumer implements RocketMQListener<String> {
    @Autowired
    private AccountService accountService;

    @Override
    public void onMessage(String json) {
        AccountMessage am = JsonUtil.from(json, AccountMessage.class);
        accountService.decrease(am.getUserId(), am.getMoney());
    }
}
