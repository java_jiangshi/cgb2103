package cn.tedu.order.service;

import cn.tedu.order.entity.AccountMessage;
import cn.tedu.order.entity.Order;
import cn.tedu.order.entity.TxInfo;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.mapper.TxMapper;
import cn.tedu.order.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;
import java.util.UUID;

@RocketMQTransactionListener
@Service
@Slf4j
public class OrderServiceImpl implements OrderService, RocketMQLocalTransactionListener {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private EasyIdClient easyIdClient;
    @Autowired
    private StorageClient storageClient;
    @Autowired
    private AccountClient accountClient;
    @Autowired
    private RocketMQTemplate t;
    @Autowired
    private TxMapper txMapper;

    @Override
    public void create(Order order) {
        // 产生一个事务id
        String xid = UUID.randomUUID().toString().replace("-","");
        // 创建AccountMessage对象，封装三个数据
        AccountMessage am = new AccountMessage(
                order.getUserId(), order.getMoney(), xid);
        // 转成 json 字符串
        String json = JsonUtil.to(am);
        // 创建 spring 的通用 message 对象，封装json字符串
        Message<String> msg = MessageBuilder.withPayload(json).build();
        // 发送事务消息
        // 会触发监听器执行本地事务
        // t.sendMessageInTransaction("order-topic", msg, 业务数据参数);
        t.sendMessageInTransaction("order-topic", msg, order);
    }

    /*
    在订单业务中，不直接执行业务数据处理，
    要由发送事务消息来触发执行
     */
    public void doCreate(Order order) {

        // 调用发号器获取订单id
        String s = easyIdClient.nextId("order_business");
        Long orderId = Long.valueOf(s);
        order.setId(orderId);
        orderMapper.create(order);

        if (Math.random() < 0.5) {
            throw new RuntimeException("模拟异常");
        }


        // 减少库存
        //storageClient.decrease(order.getProductId(),order.getCount());
        // 扣减账户
        //accountClient.decrease(order.getUserId(),order.getMoney());
    }

    // 执行本地事务
    @Transactional
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        //状态变量
        RocketMQLocalTransactionState state; //返回值
        int status; //存到数据库表
        try {
            Order order = (Order) o;
            doCreate(order);
            state = RocketMQLocalTransactionState.COMMIT;
            status = 0;
        } catch (Exception e) {
            log.error("创建订单失败", e);
            state = RocketMQLocalTransactionState.ROLLBACK;
            status = 1;
        }

        // message ---- {xid:yt3r2, userId:8, money:100}
        String json = new String((byte[]) message.getPayload());
        String xid = JsonUtil.getString(json, "xid");
        // 事务状态存储到数据库
        txMapper.insert(new TxInfo(xid, status, System.currentTimeMillis()));
        return state;
    }

    // 处理事务回查
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        String json = new String((byte[]) message.getPayload());
        String xid = JsonUtil.getString(json, "xid");
        // 从数据库查询事务状态
        TxInfo txInfo = txMapper.selectById(xid);
        if (txInfo == null) {
            return RocketMQLocalTransactionState.UNKNOWN;
        }
        switch (txInfo.getStatus()) {
            case 0: return RocketMQLocalTransactionState.COMMIT;
            case 1: return RocketMQLocalTransactionState.ROLLBACK;
            default: return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
