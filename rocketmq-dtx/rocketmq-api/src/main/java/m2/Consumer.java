package m2;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.*;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws MQClientException {
        // 新建消费者实例
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("consumer2");
        // 设置 name server
        c.setNamesrvAddr("192.168.64.141:9876");
        // 从哪订阅消息
        c.subscribe("Topic2", "*");
        // 设置消息监听器
        /*
        Orderely -- 按顺序消费
        单线程处理消息
         */
        c.setMessageListener(new MessageListenerOrderly() {
            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
                for (MessageExt msg : msgs) {
                    String s = new String(msg.getBody());
                    System.out.println("收到： "+s);
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });
        // 启动
        c.start();
    }
}
