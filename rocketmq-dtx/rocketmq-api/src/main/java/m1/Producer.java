package m1;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws MQClientException, MQBrokerException, RemotingException, InterruptedException {
        // 新建生产者实例
        DefaultMQProducer p = new DefaultMQProducer("producer1");
        // 设置name server的地址
        p.setNamesrvAddr("192.168.64.141:9876");
        // 启动 -- 和服务器建立连接
        p.start();
        // 消息数据封装到 Message 对象再发送
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            // Topic 相当于是一级分类，Tag 相当于是二级分类
            Message msg = new Message("Topic1", "Tag1", s.getBytes());
            // 延时消息，延时时长用 1 到 18 级别表示
            msg.setDelayTimeLevel(3);

            SendResult r = p.send(msg);
            System.out.println(r);
        }

    }
}
